<?php declare(strict_types=1);

namespace AppBundle\Service;

use AppBundle\Model\Elevator;

class ElevatorsService
{
    const MOVE_UP = 'up';
    const MOVE_DOWN = 'down';


    /**
     * @var StateStorage
     */
    private $storage;

    /**
     * @var array
     */
    protected $elevators = [];

    /**
     * @var int
     */
    protected $maxFloor = 0;

    public function __construct(StateStorage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @param string $indexName
     * @param Elevator $elevator
     * @param int $maxFloor
     */
    public function addElevator(string $indexName, Elevator $elevator, int $maxFloor) : void
    {
        $elevator->setName($indexName);
        $elevator->setMaxFloor($maxFloor);
        $this->maxFloor = max($this->maxFloor, $maxFloor);
        $this->elevators[] = $elevator;
    }

    public function updateCurrentStateFrmStorage()
    {
        foreach ($this->elevators as $elevator) {
            $this->storage->update($elevator);
        }
    }

    /**
     * @return int
     */
    public function getTotalFloors() : int
    {
        return $this->maxFloor;
    }

    /**
     * @return Elevator[]
     */
    public function getElevators() : array
    {
        return $this->elevators;
    }

    /**
     * @param int $floor
     * @return bool
     */
    public function checkFloor(int $floor) : bool
    {
        return $floor >= 1 && $floor <= $this->maxFloor;
    }

    /**
     * @param int $fromFloor
     * @param string $to
     * @return Elevator
     */
    public function getNearest(int $fromFloor, string $to) : Elevator
    {
        /** @var Elevator|null $select */
        $select = null;
        $calcDiff = -1;

        if ($to == self::MOVE_UP) {
            $elevators = array_filter($this->elevators, function ($elevator) use ($fromFloor) {
                /** @var Elevator $elevator */
                return $elevator->getMaxFloor() > $fromFloor;
            });
        } else {
            $elevators = $this->elevators;
        }


        /** @var Elevator $elevator */
        foreach ($elevators as $elevator) {
            if ($fromFloor <= $elevator->getMaxFloor()) {
                $diff = abs($fromFloor - $elevator->getCurrentFloor());

                if (!$select || $diff < $calcDiff) {
                    $select = $elevator;
                    $calcDiff = $diff;
                }
            }
        }

        return $select;
    }

    /**
     * @param int $fromFloor
     * @param int $toFloor
     * @return Elevator
     */
    public function move(int $fromFloor, int $toFloor) : Elevator
    {
        /** @var Elevator|null $select */
        $select = null;
        $calcDiff = -1;

        foreach ($this->elevators as $elevator) {
            /** @var Elevator $elevator */
            if ($toFloor <= $elevator->getMaxFloor()) {
                $diff = abs($fromFloor - $elevator->getCurrentFloor());

                if (!$select || $diff < $calcDiff) {
                    $select = $elevator;
                    $calcDiff = $diff;
                }
            }
        }

        $select->setMoveToFloor($toFloor);
        $this->storage->save($select);
        return $select;
    }

    /**
     * Clear data from cache.
     */
    public function clearStates()
    {
        $this->storage->clear();
    }
}
