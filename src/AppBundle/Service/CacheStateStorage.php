<?php declare(strict_types=1);

namespace AppBundle\Service;

use Symfony\Component\Cache\Adapter\AbstractAdapter;
use Symfony\Component\Cache\CacheItem;

class CacheStateStorage extends StateStorage
{
    /**
     * @var AbstractAdapter
     */
    private $adapter;

    /**
     * CacheStateStorage constructor.
     * @param AbstractAdapter $adapter
     */
    public function __construct(AbstractAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @param string $name
     * @param string $data
     * @return bool
     * @throws \Psr\Cache\InvalidArgumentException
     */
    protected function set(string $name, string $data) : bool
    {
        /** @var CacheItem $item */
        $item = $this->adapter->getItem($name);
        $item->set($data);

        return $this->adapter->save($item);
    }

    /**
     * @param string $name
     * @return string
     * @throws \Psr\Cache\InvalidArgumentException
     */
    protected function get(string $name) : string
    {
        /** @var CacheItem $item */
        $item = $this->adapter->getItem($name);

        return (string) $item->get();
    }

    /**
     * @return bool
     */
    protected function deleteAll(): bool
    {
        return $this->adapter->clear();
    }
}
