<?php declare(strict_types=1);

namespace AppBundle\Service;

use AppBundle\Model\Elevator;

abstract class StateStorage
{
    /**
     * @param Elevator $elevator
     */
    public function save(Elevator $elevator) : void
    {
        $this->set($elevator->getName(), (string) $elevator->getMoveToFloor());
    }

    /**
     * @param Elevator $elevator
     */
    public function update(Elevator $elevator) : void
    {
        $data = $this->get($elevator->getName());
        if ($data) {
            $elevator->setCurrentFloor((int) $data);
        }
    }

    /**
     * Clear full data.
     */
    public function clear()
    {
        $this->deleteAll();
    }

    /**
     * @param string $name
     * @param string $data
     * @return bool
     */
    abstract protected function set(string $name, string $data) : bool;

    /**
     * @param string $name
     * @return string
     */
    abstract protected function get(string $name) : string;

    /**
     * @return bool
     */
    abstract protected function deleteAll() : bool;
}
