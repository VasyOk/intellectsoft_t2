<?php declare(strict_types=1);

namespace AppBundle\Model;

class Elevator
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $maxFloor;

    /**
     * @var int
     */
    private $currentFloor = 1;

    /**
     * @var int|null
     */
    private $moveToFloor = null;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getMaxFloor(): int
    {
        return $this->maxFloor;
    }

    /**
     * @param int $maxFloor
     */
    public function setMaxFloor(int $maxFloor) : void
    {
        $this->maxFloor = $maxFloor;
    }

    /**
     * @return int
     */
    public function getCurrentFloor(): int
    {
        return $this->currentFloor;
    }

    /**
     * @param int $currentFloor
     */
    public function setCurrentFloor(int $currentFloor) : void
    {
        $this->currentFloor = $currentFloor;
    }

    /**
     * @return int|null
     */
    public function getMoveToFloor() : ?int
    {
        return $this->moveToFloor;
    }

    /**
     * @param int|null $moveToFloor
     */
    public function setMoveToFloor($moveToFloor) : void
    {
        $this->moveToFloor = $moveToFloor;
    }
}
