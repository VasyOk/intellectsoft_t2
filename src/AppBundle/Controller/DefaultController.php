<?php declare(strict_types=1);

namespace AppBundle\Controller;

use AppBundle\Service\ElevatorsService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 *
 * @Template()
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     *
     * @return Response
     */
    public function indexAction() : Response
    {
        $this->getElevatorManager()->clearStates();
        return $this->redirectToRoute('app.floor', ['floor' => 1]);
    }

    /**
     * @Route("/{floor}", name="app.floor")
     *
     * @param int $floor
     * @return array
     */
    public function floorAction(int $floor) : array
    {
        $em = $this->getElevatorManager();

        if (!$em->checkFloor($floor)) {
            throw new NotFoundHttpException('Floor with elevators will not find');
        }

        return [
            'currentFloor' => $floor,
            'elevatorManager' => $this->getElevatorManager(),
        ];
    }

    /**
     * @Route("/{floor}/call-{to}", name="app.call_for")
     *
     * @param int $floor
     * @param string $to
     * @return array
     */
    public function callForAction(int $floor, string $to) : array
    {
        $em = $this->getElevatorManager();

        if (!$em->checkFloor($floor)) {
            throw new NotFoundHttpException('Floor with elevators will not find');
        }

        return [
            'to' => $to,
            'currentFloor' => $floor,
            'elevator' => $em->getNearest($floor, $to),
        ];
    }

    /**
     * @Route("/{floor}/move/{to}", name="app.move_to")
     *
     * @param int $floor
     * @param int $to
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function moveAction(int $floor, int $to)
    {
        $em = $this->getElevatorManager();

        if (!$em->checkFloor($floor)) {
            throw new NotFoundHttpException('Floor with elevators will not find');
        }

        $em->move($floor, $to);

        return $this->redirectToRoute('app.floor', ['floor' => $floor]);
    }

    /**
     * @return ElevatorsService
     */
    protected function getElevatorManager() : ElevatorsService
    {
        return $this->get(ElevatorsService::class);
    }
}
