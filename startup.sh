#!/usr/bin/env bash

docker-compose build && docker-compose up -d
docker-compose exec -T --user www-data php-fpm composer install
